const transporter = require('./transport.js');

const sendNewsletterMail = async (newsletterPayload) => {
    await transporter.sendMail({
        from: process.env.MAILADDRESS,
        to: newsletterPayload.email,
        subject: "Book newsletter!",
        text: `The new book ${newsletterPayload.book.name} written by ${newsletterPayload.book.author} has been added to our collection!`
    })
};

module.exports = {
    sendNewsletterMail
}
