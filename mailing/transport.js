const nodemailer = require('nodemailer');
const {
  getSecret
} = require('docker-secret');

const auth = {
  user: process.env.MAILUSER,
  pass: process.env.NODE_ENV === 'development' ? process.env.MAILPASSWORD : getSecret(process.env.MAILPASSWORD_FILE)
};

const transport = nodemailer.createTransport({
  host: process.env.MAILHOST,
  port: 465,
  secure: true, // true for 465, false for other ports
  auth
});

module.exports = transport;