# Moby Mailing Microservice

The Mailing microservice for the Moby Lib project.

## REST API

|Method|Path|Description|Req Body|Res Body|Minimum Role|
|---|---|---|---|---|---|
| POST | /api/newsletter | **INTERNAL**. Used by other microservice to send an email to all emails in the mailing list about the new book that was added to the library. | {"mailingList": [<email_string>, ...], "book": {"name": <book_name>, "author": <book_author>}} |  | None |

## Deployment

### Monorepo configurations - RECOMMENDED

[monorepo](https://gitlab.com/moby-lib/moby-monorepo)

### Using Docker - NOT RECOMMENDED
```
sudo docker network create microservices-net (if not already created)
sudo docker run --name moby-mailing-microservice --network="microservices-net" --env-file .env -p 3003:3003 registry.gitlab.com/moby-lib/moby-mailing-microservice:1.0
```