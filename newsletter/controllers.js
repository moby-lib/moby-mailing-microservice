const Router = require('express').Router();

const {
    notify
} = require('./services.js');

Router.post('/', (req, res) => {
    const {
        mailingList,
        book
    } = req.body;

    notify(mailingList, book);

    res.status(204).end();
});

module.exports = Router;