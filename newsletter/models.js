class NewsletterPayload {
    constructor(email, book) {
        this.email = email;
        this.book = {
            name: book.name,
            author: book.author
        }
    }
}

module.exports = {
    NewsletterPayload
}